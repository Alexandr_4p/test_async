from scapy.all import *

def capture_packets():
    '''
    sniff - захватывает пакеты сетвого трафика
    wrpcap - сохраняет захваченные пакеты в файл (packet.pcap)
    '''
    packets = sniff(count=100)
    wrpcap('packet.pcap', packets)

if __name__ == '__main__':
    capture_packets()